<?php
namespace App\Providers\Social;

class SocialShare
{
    /**
     * Url of page to share
     *
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $media;

    /**
     * @param $url
     * @param $media
     * @return $this
     */
    public function page($url, $media = null){
        $this->media = $media;
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function twitter(){
        $base = config('socialshare.services.twitter.uri');
        $url = $base . '?url=' . $this->url;
        return $url;
    }

    /**
     * @return string
     */
    public function facebook(){
        $url = config('socialshare.services.facebook.uri') . $this->url;
        return $url;
    }

    /**
     * @return string
     */
    public function line(){
        $url = config('socialshare.services.line.uri') . $this->url;
        return $url;
    }

    /**
     * @return string
     */
    public function pinterest(){
        $base = config('socialshare.services.pinterest.uri');
        $url = $base . $this->url . '&media=' . $this->media;
        return $url;
    }
}