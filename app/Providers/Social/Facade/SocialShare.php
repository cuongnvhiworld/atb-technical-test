<?php

namespace App\Providers\Social\Facade;

use Illuminate\Support\Facades\Facade;

class SocialShare extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'SocialShare';
    }
}
