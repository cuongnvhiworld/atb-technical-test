<?php

namespace App\Providers;

use App\Providers\Social\SocialShare;
use Carbon\Laravel\ServiceProvider;

class SocialShareServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('SocialShare', function ($app) {
            return new SocialShare();
        });
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return array
     */
    public function boot()
    {
        return [SocialShare::class];
    }
}