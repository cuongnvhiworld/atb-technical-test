<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .share-btn {
                padding: 10px 15px !important;
                background: #fff !important;
                color: #0091be !important;
                font-weight : bold;
                text-decoration: none;
            }

            .share-btn:hover{
                background: #0091be !important;
                color: #fff !important;
            }
        </style>
        <script
                src="http://code.jquery.com/jquery-3.3.1.js"
                integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
                crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel
                </div>

                <div class="links">
                    <a class="share-btn social-button" href="{{ SocialShare::page(request()->fullUrl())->twitter() }}">Twitter</a>
                    <a class="share-btn social-button" href="{{ SocialShare::page(request()->fullUrl())->facebook() }}">Facebook</a>
                    <a class="share-btn social-button" href="{{ SocialShare::page(request()->fullUrl())->line() }}">Line</a>
                    <a class="share-btn social-button" href="{{ SocialShare::page(request()->fullUrl())->pinterest() }}">Pinterest</a>
                    <a href="https://bitbucket.org/cuongnvhiworld/atb-technical-test/src/master/" target="_blank">Bitbucket</a>
                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        $(document).on('click', '.social-button', function (e) {
            let popupSize = {
                width: 780,
                height: 550
            };
            let verticalPos = Math.floor(($(window).width() - popupSize.width) / 2),
                horisontalPos = Math.floor(($(window).height() - popupSize.height) / 2);

            let popup = window.open($(this).prop('href'), 'social',
                'width=' + popupSize.width + ',height=' + popupSize.height +
                ',left=' + verticalPos + ',top=' + horisontalPos +
                ',location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1');

            if (popup) {
                popup.focus();
                e.preventDefault();
            }

        });
    </script>
</html>
