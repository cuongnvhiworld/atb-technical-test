<?php
return [
    'services' => [
        'twitter' => [
            'uri' => 'https://twitter.com/intent/tweet',
        ],
        'facebook' => [
            'uri' => 'https://www.facebook.com/sharer/sharer.php?u=',
        ],
        'line' => [
            'uri' => 'https://social-plugins.line.me/lineit/share?url=',
        ],
        'pinterest' => [
            'uri' => 'https://www.pinterest.com/pin/create/button/?url=',
        ],
    ],
];